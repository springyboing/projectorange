package uk.co.accio.cfdemo

class Page {

    static constraints = {
        title()
        value()
        footer()
    }

    String title
    String value
    String footer
}
