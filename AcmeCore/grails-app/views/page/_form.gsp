<%@ page import="uk.co.accio.cfdemo.Page" %>



<div class="fieldcontain ${hasErrors(bean: pageInstance, field: 'footer', 'error')} ">
	<label for="footer">
		<g:message code="page.footer.label" default="Footer" />
		
	</label>
	<g:textField name="footer" value="${pageInstance?.footer}" />
</div>

<div class="fieldcontain ${hasErrors(bean: pageInstance, field: 'title', 'error')} ">
	<label for="title">
		<g:message code="page.title.label" default="Title" />
		
	</label>
	<g:textField name="title" value="${pageInstance?.title}" />
</div>

<div class="fieldcontain ${hasErrors(bean: pageInstance, field: 'value', 'error')} ">
	<label for="value">
		<g:message code="page.value.label" default="Value" />
		
	</label>
	<g:textField name="value" value="${pageInstance?.value}" />
</div>

